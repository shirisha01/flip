package com.stampit.transportation.model

data class OnGoingTrips (
    var source: String,
    var destination: String,
    var DateTime: String,
    var ServieNumber: String
)