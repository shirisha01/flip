package com.stampit.transportation.model;

public class Booking {
    Integer CustomerId, MaterialId, UOMId,Id;
    String DescriptionOfGoods, LoadingPoint, LoadingPinCode, LoadingAddress, ContactNumberAtSource, ContactNameAtSource,
            DeliveryPoint,
            DeliveryPinCode, DeliveryAddress, ContactNumberAtDelivery, ServiceRequiredDate, ReferVehicle;
    Float TotalWeight, ApproximateDistance, ApproximateTravelTime, TotalDistance, PerKmCharge, DriverBattaPerDay, ServiceTax,
            Toll, ParkingCharges, TransportCost, HamaliCharges, OtherCharges, EstimatedTransportCost, EstimatedServiceTax,
            EstimatedTotalCostAfterTax, EstimatedOtherCharges,
            EstimatedDriverBatta, EstimatedTotalToll, EstimatedHamaliCharges, EstimatedGrandTotal;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Integer getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(Integer customerId) {
        CustomerId = customerId;
    }

    public Integer getMaterialId() {
        return MaterialId;
    }

    public void setMaterialId(Integer materialId) {
        MaterialId = materialId;
    }

    public Integer getUOMId() {
        return UOMId;
    }

    public void setUOMId(Integer UOMId) {
        this.UOMId = UOMId;
    }

    public String getDescriptionOfGoods() {
        return DescriptionOfGoods;
    }

    public void setDescriptionOfGoods(String descriptionOfGoods) {
        DescriptionOfGoods = descriptionOfGoods;
    }

    public String getLoadingPoint() {
        return LoadingPoint;
    }

    public void setLoadingPoint(String loadingPoint) {
        LoadingPoint = loadingPoint;
    }

    public String getLoadingPinCode() {
        return LoadingPinCode;
    }

    public void setLoadingPinCode(String loadingPinCode) {
        LoadingPinCode = loadingPinCode;
    }

    public String getLoadingAddress() {
        return LoadingAddress;
    }

    public void setLoadingAddress(String loadingAddress) {
        LoadingAddress = loadingAddress;
    }

    public String getContactNumberAtSource() {
        return ContactNumberAtSource;
    }

    public void setContactNumberAtSource(String contactNumberAtSource) {
        ContactNumberAtSource = contactNumberAtSource;
    }

    public String getContactNameAtSource() {
        return ContactNameAtSource;
    }

    public void setContactNameAtSource(String contactNameAtSource) {
        ContactNameAtSource = contactNameAtSource;
    }

    public String getDeliveryPoint() {
        return DeliveryPoint;
    }

    public void setDeliveryPoint(String deliveryPoint) {
        DeliveryPoint = deliveryPoint;
    }

    public String getDeliveryPinCode() {
        return DeliveryPinCode;
    }

    public void setDeliveryPinCode(String deliveryPinCode) {
        DeliveryPinCode = deliveryPinCode;
    }

    public String getDeliveryAddress() {
        return DeliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        DeliveryAddress = deliveryAddress;
    }

    public String getContactNumberAtDelivery() {
        return ContactNumberAtDelivery;
    }

    public void setContactNumberAtDelivery(String contactNumberAtDelivery) {
        ContactNumberAtDelivery = contactNumberAtDelivery;
    }

    public String getServiceRequiredDate() {
        return ServiceRequiredDate;
    }

    public void setServiceRequiredDate(String serviceRequiredDate) {
        ServiceRequiredDate = serviceRequiredDate;
    }

    public String getReferVehicle() {
        return ReferVehicle;
    }

    public void setReferVehicle(String referVehicle) {
        ReferVehicle = referVehicle;
    }

    public Float getTotalWeight() {
        return TotalWeight;
    }

    public void setTotalWeight(Float totalWeight) {
        TotalWeight = totalWeight;
    }

    public Float getApproximateDistance() {
        return ApproximateDistance;
    }

    public void setApproximateDistance(Float approximateDistance) {
        ApproximateDistance = approximateDistance;
    }

    public Float getApproximateTravelTime() {
        return ApproximateTravelTime;
    }

    public void setApproximateTravelTime(Float approximateTravelTime) {
        ApproximateTravelTime = approximateTravelTime;
    }

    public Float getTotalDistance() {
        return TotalDistance;
    }

    public void setTotalDistance(Float totalDistance) {
        TotalDistance = totalDistance;
    }

    public Float getPerKmCharge() {
        return PerKmCharge;
    }

    public void setPerKmCharge(Float perKmCharge) {
        PerKmCharge = perKmCharge;
    }

    public Float getDriverBattaPerDay() {
        return DriverBattaPerDay;
    }

    public void setDriverBattaPerDay(Float driverBattaPerDay) {
        DriverBattaPerDay = driverBattaPerDay;
    }

    public Float getServiceTax() {
        return ServiceTax;
    }

    public void setServiceTax(Float serviceTax) {
        ServiceTax = serviceTax;
    }

    public Float getToll() {
        return Toll;
    }

    public void setToll(Float toll) {
        Toll = toll;
    }

    public Float getParkingCharges() {
        return ParkingCharges;
    }

    public void setParkingCharges(Float parkingCharges) {
        ParkingCharges = parkingCharges;
    }

    public Float getTransportCost() {
        return TransportCost;
    }

    public void setTransportCost(Float transportCost) {
        TransportCost = transportCost;
    }

    public Float getHamaliCharges() {
        return HamaliCharges;
    }

    public void setHamaliCharges(Float hamaliCharges) {
        HamaliCharges = hamaliCharges;
    }

    public Float getOtherCharges() {
        return OtherCharges;
    }

    public void setOtherCharges(Float otherCharges) {
        OtherCharges = otherCharges;
    }

    public Float getEstimatedTransportCost() {
        return EstimatedTransportCost;
    }

    public void setEstimatedTransportCost(Float estimatedTransportCost) {
        EstimatedTransportCost = estimatedTransportCost;
    }

    public Float getEstimatedServiceTax() {
        return EstimatedServiceTax;
    }

    public void setEstimatedServiceTax(Float estimatedServiceTax) {
        EstimatedServiceTax = estimatedServiceTax;
    }

    public Float getEstimatedTotalCostAfterTax() {
        return EstimatedTotalCostAfterTax;
    }

    public void setEstimatedTotalCostAfterTax(Float estimatedTotalCostAfterTax) {
        EstimatedTotalCostAfterTax = estimatedTotalCostAfterTax;
    }

    public Float getEstimatedOtherCharges() {
        return EstimatedOtherCharges;
    }

    public void setEstimatedOtherCharges(Float estimatedOtherCharges) {
        EstimatedOtherCharges = estimatedOtherCharges;
    }

    public Float getEstimatedDriverBatta() {
        return EstimatedDriverBatta;
    }

    public void setEstimatedDriverBatta(Float estimatedDriverBatta) {
        EstimatedDriverBatta = estimatedDriverBatta;
    }

    public Float getEstimatedTotalToll() {
        return EstimatedTotalToll;
    }

    public void setEstimatedTotalToll(Float estimatedTotalToll) {
        EstimatedTotalToll = estimatedTotalToll;
    }

    public Float getEstimatedHamaliCharges() {
        return EstimatedHamaliCharges;
    }

    public void setEstimatedHamaliCharges(Float estimatedHamaliCharges) {
        EstimatedHamaliCharges = estimatedHamaliCharges;
    }

    public Float getEstimatedGrandTotal() {
        return EstimatedGrandTotal;
    }

    public void setEstimatedGrandTotal(Float estimatedGrandTotal) {
        EstimatedGrandTotal = estimatedGrandTotal;
    }
}
