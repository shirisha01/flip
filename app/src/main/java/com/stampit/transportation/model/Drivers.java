package com.stampit.transportation.model;

import java.math.BigInteger;

public class Drivers {
    String LicenseNo,NameoftheDriver,Relation,RelationName,Address1,Address2,IssuesOn,DateofFirstIssues,DateofBirth,CreatedDate,UpdatedDate;
    String MobileNumber;
    Integer City,LicenceType,RefrenceNo,OrigionalLA,BloodGroup,IsActive,CreatedByUserId,UpdatedByUserId,VillageId,Id;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getLicenseNo() {
        return LicenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        LicenseNo = licenseNo;
    }

    public String getNameoftheDriver() {
        return NameoftheDriver;
    }

    public void setNameoftheDriver(String nameoftheDriver) {
        NameoftheDriver = nameoftheDriver;
    }

    public String getRelation() {
        return Relation;
    }

    public void setRelation(String relation) {
        Relation = relation;
    }

    public String getRelationName() {
        return RelationName;
    }

    public void setRelationName(String relationName) {
        RelationName = relationName;
    }

    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String address1) {
        Address1 = address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public String getIssuesOn() {
        return IssuesOn;
    }

    public void setIssuesOn(String issuesOn) {
        IssuesOn = issuesOn;
    }

    public String getDateofFirstIssues() {
        return DateofFirstIssues;
    }

    public void setDateofFirstIssues(String dateofFirstIssues) {
        DateofFirstIssues = dateofFirstIssues;
    }

    public String getDateofBirth() {
        return DateofBirth;
    }

    public void setDateofBirth(String dateofBirth) {
        DateofBirth = dateofBirth;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        UpdatedDate = updatedDate;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public Integer getCity() {
        return City;
    }

    public void setCity(Integer city) {
        City = city;
    }

    public Integer getLicenceType() {
        return LicenceType;
    }

    public void setLicenceType(Integer licenceType) {
        LicenceType = licenceType;
    }

    public Integer getRefrenceNo() {
        return RefrenceNo;
    }

    public void setRefrenceNo(Integer refrenceNo) {
        RefrenceNo = refrenceNo;
    }

    public Integer getOrigionalLA() {
        return OrigionalLA;
    }

    public void setOrigionalLA(Integer origionalLA) {
        OrigionalLA = origionalLA;
    }

    public Integer getBloodGroup() {
        return BloodGroup;
    }

    public void setBloodGroup(Integer bloodGroup) {
        BloodGroup = bloodGroup;
    }

    public Integer getIsActive() {
        return IsActive;
    }

    public void setIsActive(Integer isActive) {
        IsActive = isActive;
    }

    public Integer getCreatedByUserId() {
        return CreatedByUserId;
    }

    public void setCreatedByUserId(Integer createdByUserId) {
        CreatedByUserId = createdByUserId;
    }

    public Integer getUpdatedByUserId() {
        return UpdatedByUserId;
    }

    public void setUpdatedByUserId(Integer updatedByUserId) {
        UpdatedByUserId = updatedByUserId;
    }

    public Integer getVillageId() {
        return VillageId;
    }

    public void setVillageId(Integer villageId) {
        VillageId = villageId;
    }
}
