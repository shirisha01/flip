package com.stampit.transportation.fragments

import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import com.stampit.transportation.R

class LicenseImageViewFragment : AppCompatActivity() {
    var mContext: Context? = null
    var rootView: View? = null
    override fun onCreate(savedInstanceState: Bundle?,) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_licenseimage_view)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_back)
        initViews()
    }

    private fun initViews() {

        setViews()
    }

    private fun setViews() {}
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}