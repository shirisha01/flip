package com.stampit.transportation.uitls;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.stampit.transportation.R;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DataManager<T> {
    private boolean isProgressDialogRunning;
    private Dialog mDialog;
    private static final DataManager instance = new DataManager();
    public static final String User_Registration = "User_Registration";
    private final Map<String, T> dataMap = new ConcurrentHashMap<>();

    @NonNull
    public static DataManager getInstance() {
        return instance;
    }
    public void hideProgressMessage() {
        isProgressDialogRunning = true;
        try {
            if (mDialog != null)
                mDialog.dismiss();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void showProgressMessage(Context dialogActivity, String msg) {
        try {
            if (isProgressDialogRunning) {
                hideProgressMessage();
            }
            isProgressDialogRunning = true;
            mDialog = new Dialog(dialogActivity, R.style.MyMaterialTheme);
            mDialog.setContentView(R.layout.custom_progress_bar);
//            mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(dialogActivity,R.color.white_trance)));
            mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            TextView textView = mDialog.findViewById(R.id.loadingText);
            textView.setVisibility(View.VISIBLE);
            if (msg != null)
                textView.setText(Html.fromHtml(msg));
            else textView.setVisibility(View.GONE);
            WindowManager.LayoutParams lp = mDialog.getWindow().getAttributes();
            lp.dimAmount = 0.8f;
            mDialog.getWindow().setAttributes(lp);
            mDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mDialog.setCancelable(false);
            mDialog.setCanceledOnTouchOutside(false);
            mDialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public synchronized void addData(final String type, final T data) {
        dataMap.put(type, data);
    }

    public synchronized T getDataFromManager(final String type) {
        return dataMap.get(type);
    }

    public void deleteData(final String type) {
        if (dataMap.get(type) != null) {
            dataMap.remove(type);
        }
    }
}
