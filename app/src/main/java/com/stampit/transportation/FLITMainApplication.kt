package com.stampit.transportation

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Application
import android.os.Build
import android.os.StrictMode
import com.stampit.hmcsales.cloudhelper.Config
import com.msg91.sendotpandroid.library.internal.SendOTP
import com.stampit.transportation.cloudhelper.ApplicationThread


class FLITMainApplication : Application() {

    @SuppressLint("ObsoleteSdkInt")
    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    override fun onCreate() {
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        if (Config.DEVELOPER_MODE && Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder().detectAll().penaltyDialog().build())
            StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder().detectAll().penaltyDeath().build())
        }
        super.onCreate()
        context = this
        ApplicationThread.start()
        SendOTP.initializeApp(this)


    }

    companion object {

        var context: FLITMainApplication? = null

    }



    override fun onTerminate() {
        ApplicationThread.stop()
        super.onTerminate()
    }


}