package com.stampit.transportation.database;


class Queries {
    companion object {

        private var instance: Queries? = null

        fun getInstance(): Queries {
            if (instance == null) {
                instance = Queries()
            }
            return instance!!
        }
    }

    fun deleteTableData(): String {
        return "delete from %s"
    }

    fun UpgradeCount(): String {
        return "SELECT count(*) FROM Users"
    }

    fun getBookingDetails(driverId:String):String{
        return "select Id,CustomerId,LoadingAddress,DeliveryAddress,strftime('%d/%m/%Y', ServiceRequiredDate) as ServiceRequiredDate" +
                " from Booking where DriverId='"+driverId+"'"
    }

    fun getBookingDetailsB(Id:Int):String{
        return "select * from Booking where Id='"+Id+"'"
    }

    fun getMaterialName(materialId:Int):String{
        return "select * from Material where Id='"+materialId+"'"
    }

    fun getDriverProfileDetails(driverId: String):String{
        return "select D.NameoftheDriver,D.Address1,D.LicenseNo,L.Name," +
                "strftime('%d/%m/%Y', D.DateofBirth) as DateofBirth" +
                ",D.MobileNumber from Drivers D " +
                "Inner join  LookupDtl L on D.BloodGroup=L.Id\n" +
                "where D.Id='$driverId'"
    }

}