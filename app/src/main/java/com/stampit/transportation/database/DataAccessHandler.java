package com.stampit.transportation.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;


import com.stampit.transportation.cloudhelper.ApplicationThread;
import com.stampit.transportation.model.Booking;
import com.stampit.transportation.model.Drivers;
import com.stampit.transportation.model.Material;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DataAccessHandler<T> {

    private static final String LOG_TAG = DataAccessHandler.class.getName();

    private Context context;
    private SQLiteDatabase mDatabase;
    private int value;
    private List<ContentValues> cv;
    private String tableName;


    public DataAccessHandler(Context context) {
        this.context = context;
        try {
            mDatabase = WeighbridgeDatabase.openDataBaseNew();
            //DataBaseUpgrade.upgradeDataBase(context, mDatabase);
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }


    public DataAccessHandler(final Context context, boolean firstTime) {
        this.context = context;
        try {
            mDatabase = WeighbridgeDatabase.openDataBaseNew();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeDataBase() {
       /* if (mDatabase != null)
            mDatabase.close();*/
    }


    public synchronized void updateData(String tableName, List<LinkedHashMap> list, Boolean isClaues, String whereCondition, final ApplicationThread.OnComplete<String> oncomplete) {
        boolean isUpdateSuccess = false;
        int checkCount = 0;
        try {
            for (int i = 0; i < list.size(); i++) {
                checkCount++;
                List<Map.Entry> entryList = new ArrayList<Map.Entry>((list.get(i)).entrySet());
                String query = "update " + tableName + " set ";
                String namestring = "";

                System.out.println("\n==> Size of Entry list: " + entryList.size());
                StringBuffer columns = new StringBuffer();
                for (Map.Entry temp : entryList) {
                    columns.append(temp.getKey());
                    columns.append("='");
                    columns.append(temp.getValue());
                    columns.append("',");
                }

                namestring = columns.deleteCharAt(columns.length() - 1).toString();
                query = query + namestring + "" + whereCondition;
                mDatabase.execSQL(query);
                isUpdateSuccess = true;
                Log.v(LOG_TAG, "@@@ query for Plantation " + query);
            }
        } catch (Exception e) {
            checkCount++;
            e.printStackTrace();
            isUpdateSuccess = false;
        } finally {
            closeDataBase();
            if (checkCount == list.size()) {
                if (isUpdateSuccess) {
                    Log.v(LOG_TAG, "@@@ data updated successfully for " + tableName);
                    oncomplete.execute(true, null, "data updated successfully for " + tableName);
                } else {
                    oncomplete.execute(false, null, "data updation failed for " + tableName);
                }
            }
        }
    }

    public String getCountValue(String query) {
        Cursor mOprQuery = null;
        try {
            mOprQuery = mDatabase.rawQuery(query, null);
            if (mOprQuery != null && mOprQuery.moveToFirst()) {
                return mOprQuery.getString(0);
            }

            return null;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            mOprQuery.close();
            closeDataBase();
        }
        return "";
    }

    public void executeRawQuery(String query) {
        try {
            if (mDatabase != null) {
                mDatabase.execSQL(query);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
    }

    public synchronized void deleteRow(String tableName, String columnName, String value, boolean isWhere, final ApplicationThread.OnComplete<String> onComplete) {
        boolean isDataDeleted = true;

        try {

            String query = "delete from " + tableName;
            if (isWhere) {
                query = query + " where " + columnName + " = '" + value + "'";
            }
            mDatabase.execSQL(query);
        } catch (Exception e) {
            isDataDeleted = false;
            Log.e(LOG_TAG, "@@@ master data deletion failed for " + tableName + " error is " + e.getMessage());
            onComplete.execute(false, null, "master data deletion failed for " + tableName + " error is " + e.getMessage());
        } finally {
            closeDataBase();

            if (isDataDeleted) {
                Log.v(LOG_TAG, "@@@ master data deleted successfully for " + tableName);
                onComplete.execute(true, null, "master data deleted successfully for " + tableName);
            }

        }
    }

    public synchronized void insertData(String tableName, List<LinkedHashMap> mapList, final ApplicationThread.OnComplete<String> onComplete) {
        insertData(false, tableName, mapList, onComplete);
    }

    public synchronized void insertData(boolean fromMaster, String tableName, List<LinkedHashMap> mapList, final ApplicationThread.OnComplete<String> onComplete) {
        int checkCount = 0;
        try {
            List<ContentValues> values1 = new ArrayList<>();
            for (int i = 0; i < mapList.size(); i++) {
                checkCount++;
                List<LinkedHashMap.Entry> entryList = new ArrayList<>((mapList.get(i)).entrySet());

                ContentValues contentValues = new ContentValues();
                for (LinkedHashMap.Entry temp : entryList) {
                    String keyToInsert = temp.getKey().toString();

                    if (keyToInsert.equalsIgnoreCase("ServerUpdatedStatus")) {
                        contentValues.put(keyToInsert, "1");
                    } else {
                        contentValues.put(temp.getKey().toString(), temp.getValue().toString());
                    }
                }
                values1.add(contentValues);
            }
            Log.v(LOG_TAG, "@@@@ log check " + checkCount + " here " + values1.size());
            boolean hasError = bulkInsertToTable(values1, tableName);
            if (hasError) {
                Log.v(LOG_TAG, "@@@ Error while inserting data ");
                if (null != onComplete) {
                    onComplete.execute(false, "failed to insert data", "");
                }
            } else {
                Log.v(LOG_TAG, "@@@ data inserted successfully for table :" + tableName);
                if (null != onComplete) {
                    onComplete.execute(true, "data inserted successfully", "");
                }
            }
        } catch (Exception e) {
            checkCount++;
            e.printStackTrace();
            Log.v(LOG_TAG, "@@@@ exception log check " + checkCount + " here " + mapList.size());
            if (checkCount == mapList.size()) {
                if (null != onComplete)
                    onComplete.execute(false, "data insertion failed", "" + e.getMessage());
            }
        } finally {
            closeDataBase();
        }


    }

    public boolean bulkInsertToTable(List<ContentValues> cv, final String tableName) {
        this.cv = cv;
        this.tableName = tableName;
        boolean isError = false;
        mDatabase.beginTransaction();
        try {
            for (int i = 0; i < cv.size(); i++) {
                ContentValues stockResponse = cv.get(i);
                long id = mDatabase.insert(tableName, null, stockResponse);
                if (id < 0) {
                    isError = true;
                }
            }
            mDatabase.setTransactionSuccessful();
        } finally {
            mDatabase.endTransaction();
        }
        return isError;
    }

    public Drivers getDriverWithMobileNumber(String mobileNumber) {
        String selectQuery = "SELECT  Id,NameoftheDriver FROM Drivers where MobileNumber ='" + mobileNumber + "'";
        Log.v("@@@query", "" + selectQuery);
        Drivers users = null;
        Cursor cursor = null;

        try {
            cursor = mDatabase.rawQuery(selectQuery, null);
        } catch (SQLiteException e) {
            return users;
        }

        if (cursor.moveToFirst()) {
            do {
                users = new Drivers();
                users.setId(cursor.getInt(cursor.getColumnIndex("Id")));
                users.setNameoftheDriver(cursor.getString(cursor.getColumnIndex("NameoftheDriver")));

            } while (cursor.moveToNext());
        }


        if (users == null) {
            return null;
        }
        return users;
    }

    public ArrayList<Booking> getBookingDetails(String query) {
        Booking booking = null;
        ArrayList<Booking> bookingArrayList = new ArrayList<>();
        Cursor cursor = null;
        Log.v(LOG_TAG, "Booking Details query" + query);
        try {
            cursor = mDatabase.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    booking = new Booking();
                    booking.setId(cursor.getInt(cursor.getColumnIndex("Id")));
                    booking.setCustomerId(cursor.getInt(cursor.getColumnIndex("CustomerId")));
                    booking.setLoadingAddress(cursor.getString(cursor.getColumnIndex("LoadingAddress")));
                    booking.setDeliveryAddress(cursor.getString(cursor.getColumnIndex("DeliveryAddress")));
                    booking.setServiceRequiredDate(cursor.getString(cursor.getColumnIndex("ServiceRequiredDate")));
                    bookingArrayList.add(booking);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bookingArrayList;
    }

    public Booking getBooking(String Query) {

        Log.v("@@@query", "" + Query);
        Booking booking = null;
        Cursor cursor = null;

        try {
            cursor = mDatabase.rawQuery(Query, null);
        } catch (SQLiteException e) {
            return booking;
        }

        if (cursor.moveToFirst()) {
            do {
                booking = new Booking();
                booking.setLoadingPoint(cursor.getString(cursor.getColumnIndex("LoadingPoint")));
                booking.setMaterialId(Integer.parseInt(cursor.getString(cursor.getColumnIndex("MaterialId"))));
                booking.setContactNumberAtDelivery(cursor.getString(cursor.getColumnIndex("ContactNumberAtDelivery")));
                booking.setContactNumberAtSource(cursor.getString(cursor.getColumnIndex("ContactNumberAtSource")));
                booking.setContactNameAtSource(cursor.getString(cursor.getColumnIndex("ContactNameAtSource")));
                booking.setServiceRequiredDate(cursor.getString(cursor.getColumnIndex("ServiceRequiredDate")));
                booking.setDeliveryPinCode(cursor.getString(cursor.getColumnIndex("DeliveryPinCode")));
                booking.setDeliveryAddress(cursor.getString(cursor.getColumnIndex("DeliveryAddress")));


            } while (cursor.moveToNext());
        }


        if (booking == null) {
            return null;
        }
        return booking;
    }

    public Material getMaterial(String Query) {

        Log.v("@@@query", "" + Query);
        Material material = null;
        Cursor cursor = null;

        try {
            cursor = mDatabase.rawQuery(Query, null);
        } catch (SQLiteException e) {
            return material;
        }

        if (cursor.moveToFirst()) {
            do {
                material = new Material();
                material.setName(cursor.getString(cursor.getColumnIndex("Name")));
                material.setCode(cursor.getString(cursor.getColumnIndex("Code")));


            } while (cursor.moveToNext());
        }


        if (material == null) {
            return null;
        }
        return material;
    }


    public Drivers getDriverDetails(String Query) {

        Log.v("@@@query", "" + Query);
        Drivers drivers = null;
        Cursor cursor = null;

        try {
            cursor = mDatabase.rawQuery(Query, null);
        } catch (SQLiteException e) {
            return drivers;
        }

        if (cursor.moveToFirst()) {
            do {
                drivers = new Drivers();
                 drivers.setNameoftheDriver(cursor.getString(cursor.getColumnIndex("NameoftheDriver")));
                drivers.setAddress2(cursor.getString(cursor.getColumnIndex("Name")));
                 drivers.setAddress1(cursor.getString(cursor.getColumnIndex("Address1")));
                 drivers.setLicenseNo(cursor.getString(cursor.getColumnIndex("LicenseNo")));
                 drivers.setMobileNumber(cursor.getString(cursor.getColumnIndex("MobileNumber")));
                 drivers.setDateofBirth(cursor.getString(cursor.getColumnIndex("DateofBirth")));



            } while (cursor.moveToNext());
        }


        if (drivers == null) {
            return null;
        }
        return drivers;
    }

}

