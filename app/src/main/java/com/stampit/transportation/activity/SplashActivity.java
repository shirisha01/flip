package com.stampit.transportation.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.stampit.transportation.HomeScreen;
import com.stampit.transportation.R;
import com.stampit.transportation.cloudhelper.ApplicationThread;
import com.stampit.transportation.cloudhelper.DataSyncHelper;
import com.stampit.transportation.common.CommonConstants;
import com.stampit.transportation.common.CommonUtils;
import com.stampit.transportation.database.DataAccessHandler;

import com.stampit.transportation.database.Queries;
import com.stampit.transportation.database.WeighbridgeDatabase;
import com.stampit.transportation.uitls.PrefUtil;

public class SplashActivity extends AppCompatActivity {
    ImageView imageView;
    Animation topAnim,bottomAnim;
    SharedPreferences sharedPreferences;
    public static final String LOG_TAG = SplashActivity.class.getName();

    private WeighbridgeDatabase flitDatabase;
    private String[] PERMISSIONS_REQUIRED = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.CAMERA,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
        imageView = findViewById(R.id.imageLogo);
        bottomAnim = AnimationUtils.loadAnimation(this,R.anim.bottom_animation);

        imageView.setAnimation(bottomAnim);

        sharedPreferences = getSharedPreferences("appprefs", MODE_PRIVATE);
        if (!CommonUtils.isNetworkAvailable(this)) {
            Toast.makeText(this, "Please check your network connection", Toast.LENGTH_LONG).show();
            // UiUtils.showCustomToastMessage("Please check your network connection", this, 1);
        } else {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !CommonUtils.areAllPermissionsAllowed(this, PERMISSIONS_REQUIRED)) {
                ActivityCompat.requestPermissions(this, PERMISSIONS_REQUIRED, CommonUtils.PERMISSION_CODE);
            } else {
                try {
                    flitDatabase = WeighbridgeDatabase.getWeightDatabase(this);
                    flitDatabase.createDataBase();
                    dbUpgradeCall();
                } catch (Exception e) {
                    e.getMessage();
                }
                startMasterSync();
            }
        }


    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CommonUtils.PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v(LOG_TAG, "permission granted");
                    try {
                        flitDatabase = WeighbridgeDatabase.getWeightDatabase(this);
                        flitDatabase.createDataBase();
                        dbUpgradeCall();
                    } catch (Exception e) {
                        Log.e(LOG_TAG, "@@@ Error while getting master data "+e.getMessage());
                    }
                    startMasterSync();
                }
                break;
        }
    }

    public void startMasterSync() {
        if (CommonUtils.isNetworkAvailable(this) && !sharedPreferences.getBoolean(CommonConstants.INSTANCE.getIS_MASTER_SYNC_SUCCESS(),false)) {
            DataSyncHelper.performMasterSync(this, PrefUtil.INSTANCE.getBool(this, CommonConstants.INSTANCE.getIS_MASTER_SYNC_SUCCESS()), new ApplicationThread.OnComplete() {
                @Override
                public void execute(boolean success, Object result, String msg) {
                    // ProgressBar.Companion.hideProgressBar();
                    if (success) {
                        //Toast.makeText(SplashScreen.this, "Data synced successfully", Toast.LENGTH_SHORT).show();

                        sharedPreferences.edit().putBoolean(CommonConstants.INSTANCE.getIS_MASTER_SYNC_SUCCESS(), true).apply();
                        SharedPreferences preferences = getSharedPreferences("flit", MODE_PRIVATE);

                        if((preferences.contains("driverNumber"))){
                            CommonConstants.Driver_Id = preferences.getString("CID","null");
                            CommonConstants.Driver_Number =preferences.getString("driverNumber","null");
                            startActivity(new Intent(SplashActivity.this, HomeScreen.class));
                            finish();
                        }else {
                            startActivity(new Intent(SplashActivity.this, OTPActivity.class));
                            finish();
                        }                       // finish();
                    } else {
                        Log.v(LOG_TAG, "@@@ Master sync failed " + msg);
                        ApplicationThread.uiPost(LOG_TAG, "master sync message", new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(SplashActivity.this, "Please check your network connection", Toast.LENGTH_LONG).show();
                                // UiUtils.INSTANCE.showCustomToastMessage("Data syncing failed", SplashScreen.this, 1);
                                SharedPreferences preferences = getSharedPreferences("hmsales", MODE_PRIVATE);

                                if((preferences.contains("driverNumber"))){
                                    CommonConstants.Driver_Id = preferences.getString("CID","null");
                                    CommonConstants.Driver_Number =preferences.getString("driverNumber","null");
                                    Log.v("@@@LL","uu"+CommonConstants.Driver_Id);
                                    startActivity(new Intent(SplashActivity.this, HomeScreen.class));
                                    finish();
                                }else {
                                    Log.v("@@@LL","elseuu"+CommonConstants.Driver_Id);
                                    startActivity(new Intent(SplashActivity.this, OTPActivity.class));
                                    finish();
                                }
                            }
                        });
                    }
                }
            });
        } else {
            SharedPreferences preferences = getSharedPreferences("flit", MODE_PRIVATE);

            if((preferences.contains("driverNumber"))){
                CommonConstants.Driver_Id = preferences.getString("CID","null");
                CommonConstants.Driver_Number =preferences.getString("driverNumber","null");
                Log.v("@@@LL","uu"+CommonConstants.Driver_Id);
                startActivity(new Intent(SplashActivity.this, HomeScreen.class));
                finish();
            }else {
                Log.v("@@@LL","elseuu"+CommonConstants.Driver_Id);
                startActivity(new Intent(SplashActivity.this, OTPActivity.class));
                finish();
            }
        }
    }

    public void dbUpgradeCall() {
        DataAccessHandler dataAccessHandler = new DataAccessHandler(SplashActivity.this, false);
        String count = dataAccessHandler.getCountValue(Queries.Companion.getInstance().UpgradeCount());
        if (TextUtils.isEmpty(count) || Integer.parseInt(count) == 0) {
            SharedPreferences sharedPreferences = getSharedPreferences("appprefs", MODE_PRIVATE);
            sharedPreferences.edit().putBoolean(CommonConstants.IS_FRESH_INSTALL, true).apply();
        }
    }

}