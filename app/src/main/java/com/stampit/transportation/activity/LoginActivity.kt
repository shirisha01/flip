package com.stampit.transportation.activity

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.stampit.transportation.R
import com.stampit.transportation.cloudhelper.ApplicationThread
import com.stampit.transportation.cloudhelper.ApplicationThread.OnComplete
import com.stampit.transportation.cloudhelper.DataSyncHelper
import com.stampit.transportation.common.CommonConstants
import com.stampit.transportation.common.CommonConstants.IS_MASTER_SYNC_SUCCESS
import com.stampit.transportation.database.DataAccessHandler
import com.stampit.transportation.model.Drivers
import com.stampit.transportation.uitls.PrefUtil.getBool
import com.stampit.transportation.uitls.ProgressBar.Companion.hideProgressBar
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    val LOG_TAG: String = LoginActivity::class.java.getName()

    var button: Button? = null
    private var mobileNumber: EditText?=null
    var drivers: Drivers? = null
    var dataAccessHandler: DataAccessHandler<*>? = null
    var sharedPreferences: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        dataAccessHandler = DataAccessHandler<Any?>(this)
        sharedPreferences = getSharedPreferences("flit", MODE_PRIVATE)


        initviews()
        setViews()
        syncBtn!!.setOnClickListener {


        }
    }

    private fun initviews() {
        button = findViewById(R.id.button)
        mobileNumber = findViewById(R.id.mobileNumberEt)
    }

    private fun setViews() {
        button!!.setOnClickListener {

            var phoneNumer: String = mobileNumber!!.getText().toString()
            phoneNumer = phoneNumer.replace("\\s".toRegex(), "")
            drivers = dataAccessHandler!!.getDriverWithMobileNumber(phoneNumer)
            if (drivers != null) {
                CommonConstants.Driver_Id = java.lang.String.valueOf(drivers!!.getId())
                CommonConstants.Driver_Number = drivers!!.getNameoftheDriver()
                val editor: SharedPreferences.Editor = sharedPreferences!!.edit()
                editor.putString("driverNumber", phoneNumer)
                editor.putString("CID", CommonConstants.Driver_Id)

                editor.commit()
                val verification = Intent(this, OtpVerificationActivity::class.java)
                verification.putExtra("UserMobile",  mobileNumber!!.text.toString())
                startActivity(verification)
                finish()

            } else {
                Toast.makeText(this, "not a valid Driver", Toast.LENGTH_LONG).show()
            }





            
        }
    }



}