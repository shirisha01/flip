package com.stampit.transportation.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.stampit.transportation.R;
import com.stampit.transportation.cloudhelper.ApplicationThread;
import com.stampit.transportation.cloudhelper.DataSyncHelper;
import com.stampit.transportation.common.CommonConstants;
import com.stampit.transportation.database.DataAccessHandler;
import com.stampit.transportation.model.Drivers;
import com.stampit.transportation.uitls.PrefUtil;
import com.stampit.transportation.uitls.ProgressBar;

public class OTPActivity extends AppCompatActivity {
    public static final String LOG_TAG = OTPActivity.class.getName();
    SharedPreferences sharedPreferences;
    DataAccessHandler dataAccessHandler;
    Drivers drivers;
    EditText mobileNumber;
    Button button;
    FloatingActionButton syncBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_o_t_p);
        sharedPreferences = getSharedPreferences("flit", AppCompatActivity.MODE_PRIVATE);
        dataAccessHandler = new DataAccessHandler(this);
        initviews();
        setViews();


        syncBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMasterSync();
            }
        });

    }

    private void initviews() {
        button = findViewById(R.id.button);
        mobileNumber = findViewById(R.id.mobileNumberEt);
        syncBtn=findViewById(R.id.syncBtn);
    }

    private void setViews() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumer = mobileNumber.getText().toString();
                phoneNumer = phoneNumer.replaceAll("\\s", "");
                drivers = dataAccessHandler.getDriverWithMobileNumber(phoneNumer);
                if (drivers != null) {
                    CommonConstants.Driver_Id = String.valueOf(drivers.getId());
                    CommonConstants.Driver_Number = drivers.getNameoftheDriver();

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("driverNumber", phoneNumer);
                    editor.putString("CID", CommonConstants.Driver_Id);

                    editor.commit();
                    Intent verification = new Intent(OTPActivity.this, OtpVerificationActivity.class);
                    verification.putExtra("UserMobile", mobileNumber.getText().toString());
                    startActivity(verification);
                    finish();


                } else {
                    Toast.makeText(OTPActivity.this, "not a valid user", Toast.LENGTH_LONG).show();

                }

            }
        });


    }

    public void startMasterSync() {
        DataSyncHelper.performMasterSync(this,
                PrefUtil.INSTANCE.getBool(this, CommonConstants.INSTANCE.getIS_MASTER_SYNC_SUCCESS()),
                new ApplicationThread.OnComplete() {
                    @Override
                    public void execute(boolean success, Object result, String msg) {

                        if (success) {

                            ApplicationThread.uiPost(LOG_TAG, "master sync message", new Runnable() {
                                @Override
                                public void run() {
                                    sharedPreferences.edit().putBoolean(CommonConstants.INSTANCE.getIS_MASTER_SYNC_SUCCESS(), true).apply();
                                    Toast.makeText(OTPActivity.this, "Data syncing success", Toast.LENGTH_LONG).show();
                                    ProgressBar.Companion.hideProgressBar();
                                }
                            });

                        } else {
                            Log.v(LOG_TAG, "@@@ Master sync failed " + msg);
                            ApplicationThread.uiPost(LOG_TAG, "master sync message", new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(OTPActivity.this, "Data syncing failed", Toast.LENGTH_LONG).show();
                                    ProgressBar.Companion.hideProgressBar();
                                }
                            });
                        }
                    }
                });

    }

}