package com.stampit.transportation.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.stampit.transportation.R;
import com.stampit.transportation.cloudhelper.ApplicationThread;
import com.stampit.transportation.cloudhelper.DataSyncHelper;
import com.stampit.transportation.common.CommonConstants;
import com.stampit.transportation.uitls.PrefUtil;
import com.stampit.transportation.uitls.ProgressBar;

public class ExampleAct extends AppCompatActivity {
    public static final String LOG_TAG = ExampleAct.class.getName();
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example);
    }
    public void startMasterSync() {
        DataSyncHelper.performMasterSync(this,
                PrefUtil.INSTANCE.getBool(this, CommonConstants.INSTANCE.getIS_MASTER_SYNC_SUCCESS()),
                new ApplicationThread.OnComplete() {
                    @Override
                    public void execute(boolean success, Object result, String msg) {

                        if (success) {

                            ApplicationThread.uiPost(LOG_TAG, "master sync message", new Runnable() {
                                @Override
                                public void run() {
                                    sharedPreferences.edit().putBoolean(CommonConstants.INSTANCE.getIS_MASTER_SYNC_SUCCESS(), true).apply();
                                    Toast.makeText(ExampleAct.this,"Data syncing success",Toast.LENGTH_LONG).show();
                                    ProgressBar.Companion.hideProgressBar();
                                }
                            });

                        } else {
                            Log.v(LOG_TAG, "@@@ Master sync failed " + msg);
                            ApplicationThread.uiPost(LOG_TAG, "master sync message", new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(ExampleAct.this,"Data syncing failed",Toast.LENGTH_LONG).show();
                                    ProgressBar.Companion.hideProgressBar();
                                }
                            });
                        }
                    }
                });

    }

}