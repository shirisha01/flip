package com.stampit.transportation.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.stampit.transportation.R
import com.stampit.transportation.common.CommonConstants
import com.stampit.transportation.database.DataAccessHandler
import com.stampit.transportation.database.Queries
import com.stampit.transportation.maps.MapsActivityCurrentPlace
import com.stampit.transportation.model.Booking
import com.stampit.transportation.model.Material
import kotlinx.android.synthetic.main.activity_home_screen.*

class OnGoingTripViewDetailsActivity : AppCompatActivity() {

    lateinit var dataAccessHandler: DataAccessHandler<*>
    var booking: Booking? = null
    var material: Material? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_screen)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_back)
        initViews()
    }

    fun initViews() {
        val intent = intent
        CommonConstants.bookingId = intent.getIntExtra("DriverId", 0)!!

        dataAccessHandler = DataAccessHandler<Any?>(this)
        booking = dataAccessHandler.getBooking(Queries.getInstance().getBookingDetailsB(CommonConstants.bookingId))


        if (booking != null) {
            // pickUp Details
            loadingPointTv.text = booking!!.loadingPoint
            tripDateTime.text = booking!!.serviceRequiredDate.substring(0,10)
            material = dataAccessHandler.getMaterial(Queries.getInstance().getMaterialName(booking!!.materialId))
            if (material != null) {
                materialTv.text = material!!.name
            }

            contactNo.text = booking!!.contactNumberAtSource
            contactName.text = booking!!.contactNameAtSource

            // Drop Off Details
            CityTvD.text = booking!!.deliveryAddress
            stateTv.text = booking!!.contactNumberAtDelivery
            pincodeTv.text = booking!!.deliveryPinCode


        }


        startTripBtn.setOnClickListener {
            val verification = Intent(this, MapsActivityCurrentPlace::class.java)
           // verification.putExtra("ManagerNumber", "6301297261")
            startActivity(verification)
        }

        backBtn.setOnClickListener {
            finish()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}