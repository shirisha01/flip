package com.stampit.transportation;

import android.app.Application;

import com.msg91.sendotpandroid.library.internal.SendOTP;

public class TransportApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SendOTP.initializeApp(this);
    }
}
