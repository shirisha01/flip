package com.stampit.transportation.ui.home;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stampit.transportation.R;
import com.stampit.transportation.adapter.BookingFragmentAdapter;
import com.stampit.transportation.common.CommonConstants;
import com.stampit.transportation.database.DataAccessHandler;
import com.stampit.transportation.database.Queries;
import com.stampit.transportation.model.Booking;

import java.util.ArrayList;


public class BookingFragment extends Fragment {
View view;
    ArrayList<Booking> bookingArrayList = new ArrayList<>();
    DataAccessHandler dataAccessHandler;
    BookingFragmentAdapter bookingFragmentAdapter;
  RecyclerView onGoingTripsRecy;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view= inflater.inflate(R.layout.fragment_booking, container, false);
        onGoingTripsRecy=view.findViewById(R.id.onGoingTripsRecy);
        dataAccessHandler=new DataAccessHandler(getContext());
        bookingArrayList = dataAccessHandler.getBookingDetails(Queries.Companion.getInstance().getBookingDetails(CommonConstants.Driver_Id));

        Log.v("@@@L","Size"+bookingArrayList.size());
        bookingFragmentAdapter = new BookingFragmentAdapter(bookingArrayList, getContext());
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        onGoingTripsRecy.setLayoutManager(manager);
        bookingFragmentAdapter.addItems(bookingArrayList);
        onGoingTripsRecy.setAdapter(bookingFragmentAdapter);

        return view;
    }
}