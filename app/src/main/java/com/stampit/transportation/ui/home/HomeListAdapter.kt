package com.stampit.transportation.ui.home

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stampit.transportation.R
import com.stampit.transportation.activity.OnGoingTripViewDetailsActivity
import com.stampit.transportation.model.OnGoingTrips
import com.stampit.transportation.uitls.RecyclerViewClickListener

class HomeListAdapter(var onGoingList: ArrayList<OnGoingTrips>): RecyclerView.Adapter<HomeListAdapter.MyViewHolder>() {

    var  onItemClickListener: RecyclerViewClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_ongoinglist_item, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
      return onGoingList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val count = position+1
        holder.tripCount.text = "Trip#$count"
        holder.sourceTv.text = onGoingList[position].source
        holder.destinationTv.text = onGoingList[position].destination
        holder.dateTime.text =   "Date           :  "+onGoingList[position].DateTime
        holder.serviceNum.text = "Service No :  "+onGoingList[position].ServieNumber
        holder.viewDetailsBtn.setOnClickListener {
            val intent = Intent(holder.itemView.context, OnGoingTripViewDetailsActivity::class.java)
            intent.putExtra("locationPoint",onGoingList[position].source)
            intent.putExtra("destinationPoint",onGoingList[position].destination)
            intent.putExtra("dateTime",onGoingList[position].DateTime)
            holder.itemView.context.startActivity(intent)
        }

    }

    class MyViewHolder(itemView: View?):RecyclerView.ViewHolder(itemView!!) {
        val tripCount = itemView!!.findViewById(R.id.tripCount) as TextView
        val sourceTv =itemView!!.findViewById(R.id.sourcePoint) as TextView
        val destinationTv = itemView!!.findViewById(R.id.destinationPoint) as TextView
        val dateTime = itemView!!.findViewById(R.id.dataTime) as TextView
        val serviceNum = itemView!!.findViewById(R.id.serviceNumber) as TextView
        val viewDetailsBtn = itemView!!.findViewById(R.id.viewDetailsBtn) as ImageView
    }

    fun setOnItemTrip(onItemClick: RecyclerViewClickListener){
        onItemClickListener = onItemClick

    }


}