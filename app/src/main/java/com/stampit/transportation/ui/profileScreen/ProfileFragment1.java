package com.stampit.transportation.ui.profileScreen;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.stampit.transportation.R;


public class ProfileFragment1 extends Fragment {
    View view;
    Button licenceImageBtn;
    AppCompatTextView driverName, bloodGroup;
    TextView addressTv,license_noTv,expiryOn,mNo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile1, container, false);
        return view;
    }
    private void initViews(){

    }
}