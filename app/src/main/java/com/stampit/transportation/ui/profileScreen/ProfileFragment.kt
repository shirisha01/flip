package com.stampit.transportation.ui.profileScreen

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.stampit.transportation.R
import com.stampit.transportation.activity.OnGoingTripViewDetailsActivity
import com.stampit.transportation.common.CommonConstants
import com.stampit.transportation.database.DataAccessHandler
import com.stampit.transportation.database.Queries
import com.stampit.transportation.fragments.LicenseImageViewFragment
import com.stampit.transportation.model.Booking
import com.stampit.transportation.model.Drivers
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : Fragment() {
    var addressTv: TextView? = null
    var dName: TextView? = null
    var bloodgroup: TextView? = null
    var licenseNo: TextView? = null
    var dob: TextView? = null
    var pno: TextView? = null

    var licenceImageBtn: Button? = null
    lateinit var dataAccessHandler: DataAccessHandler<*>
    var drivers: Drivers? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_profile, container, false)
        dataAccessHandler = DataAccessHandler<Any?>(context)

        addressTv = root.findViewById(R.id.addressTv)
        dName = root.findViewById(R.id.dName)
        bloodgroup = root.findViewById(R.id.bloodgroup)
        licenseNo = root.findViewById(R.id.licenseNo)
        dob = root.findViewById(R.id.dob)
        pno = root.findViewById(R.id.pno)

        licenceImageBtn = root.findViewById(R.id.licenceImageBtn)
        bindData()
        return root
    }

    private fun bindData() {
        drivers = dataAccessHandler.getDriverDetails(Queries.getInstance().getDriverProfileDetails(CommonConstants.Driver_Id))
        if (drivers != null) {
            dName!!.text = drivers!!.nameoftheDriver
            bloodgroup!!.text = drivers!!.address2
            addressTv!!.text = drivers!!.address1
            licenseNo!!.text = "License No :" + drivers!!.licenseNo
            dob!!.text = "Date of Birth : " + drivers!!.dateofBirth
            pno!!.text = drivers!!.mobileNumber
        }

        licenceImageBtn!!.setOnClickListener {
            startActivity(Intent(activity, LicenseImageViewFragment::class.java))
        }
    }


}