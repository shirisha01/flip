package com.stampit.transportation.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stampit.transportation.R
import com.stampit.transportation.model.OnGoingTrips
import com.stampit.transportation.uitls.RecyclerViewClickListener

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    var onGoingTrips: RecyclerView? = null
    var onGoingList: ArrayList<OnGoingTrips> = ArrayList()
    var homeListAdapter:HomeListAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        onGoingTrips = root.findViewById(R.id.onGoingTripsRecy)
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        onGoingTrips!!.layoutManager = layoutManager
        getData()

        return root
    }

    private fun bindData() {
        homeListAdapter = HomeListAdapter(onGoingList)
        onGoingTrips!!.adapter = homeListAdapter
        homeListAdapter!!.notifyDataSetChanged()
    }

    private fun getData() {
        val trip1 = OnGoingTrips("Hyderabad","Kurnool","11 Nov 2020, 11:30AM","MU909901")
        val trip2 = OnGoingTrips("Hyderabad","Srikakulam","11 Nov 2020, 11:30AM","MU909902")
        val trip3 = OnGoingTrips("Hyderabad","RangaReddy","11 Nov 2020, 11:30AM","MU909903")
        val trip4 = OnGoingTrips("Hyderabad","Bengaluru","11 Nov 2020, 11:30AM","MU909904")
        val trip5 = OnGoingTrips("Hyderabad","Vijayawada","11 Nov 2020, 11:30AM","MU909905")
        val trip6 = OnGoingTrips("Hyderabad","Visakhapatnam","11 Nov 2020, 11:30AM","MU909906")

        onGoingList.add(trip1)
        onGoingList.add(trip2)
        onGoingList.add(trip3)
        onGoingList.add(trip4)
        onGoingList.add(trip5)
        onGoingList.add(trip6)
        bindData()
    }
}