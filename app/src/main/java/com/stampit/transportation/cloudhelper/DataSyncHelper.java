package com.stampit.transportation.cloudhelper;


import android.content.Context;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.stampit.hmcsales.cloudhelper.Config;
import com.stampit.hmcsales.cloudhelper.Log;
import com.stampit.transportation.database.DataAccessHandler;
import com.stampit.transportation.database.Queries;
import com.stampit.transportation.uitls.ProgressBar;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

public class DataSyncHelper {

    private static final String USER_AGENT = "Mozilla/5.0";
    private static final String LOG_TAG = DataSyncHelper.class.getName();
    public static String PREVIOUS_SYNC_DATE = "previous_sync_date";
    public static LinkedHashMap<String, List> dataToUpdate = new LinkedHashMap<>();
    public static int countCheck, transactionsCheck = 0;
    public static List<String> refreshtableNamesList = new ArrayList<>();
    public static int imagesCount = 0, reverseSyncTransCount = 0, innerCountCheck = 0;
    public static LinkedHashMap<String, List> refreshtransactionsDataMap = new LinkedHashMap<>();
    public static String migrationSync = "true";

    public static int tourPlan = 0 ;
    public static int trail = 0;
    public static  int promotionalPlan = 0;

    public static synchronized void performMasterSync(final Context context, final boolean firstTimeInsertFinished, final ApplicationThread.OnComplete onComplete) {

        LinkedHashMap<String, String> syncDataMap = new LinkedHashMap<>();
        syncDataMap.put("UserId", "0");
        countCheck = 0;

        final DataAccessHandler dataAccessHandler = new DataAccessHandler(context);
        ProgressBar.Companion.showProgressBar(context, "Making data ready for you...");
        CloudDataHandler.getMasterData( Config.masterSyncUrl, syncDataMap, new ApplicationThread.OnComplete<HashMap<String, List>>() {
            @Override
            public void execute(boolean success, final HashMap<String, List> masterData, String msg) {
                if (success) {
                    if (masterData != null && masterData.size() > 0) {
                        Log.INSTANCE.v(LOG_TAG, "@@@ Master sync is success and data size is " + masterData.size());
                        final Set<String> tableNames = masterData.keySet();
                        for (final String tableName : tableNames) {
                            Log.INSTANCE.v(LOG_TAG, "@@@ Delete Query " + String.format(Queries.Companion.getInstance().deleteTableData(), tableName));
                            ApplicationThread.dbPost("Master Data Sync..", "master data", new Runnable() {
                                @Override
                                public void run() {
                                    countCheck++;
                                    if (!firstTimeInsertFinished) {
                                        dataAccessHandler.deleteRow(tableName, null, null, false, new ApplicationThread.OnComplete<String>() {
                                            @Override
                                            public void execute(boolean success, String result, String msg) {
                                                if (success) {
                                                    dataAccessHandler.insertData(true, tableName, masterData.get(tableName), new ApplicationThread.OnComplete<String>() {
                                                        @Override
                                                        public void execute(boolean success, String result, String msg) {
                                                            if (success) {
                                                                Log.INSTANCE.v(LOG_TAG, "@@@ sync success for " + tableName);
                                                            } else {
                                                                Log.INSTANCE.v(LOG_TAG, "@@@ check 1 " + masterData.size() + "...pos " + countCheck);
                                                                Log.INSTANCE.v(LOG_TAG, "@@@ sync failed for " + tableName + " message " + msg);
                                                            }
                                                            if (countCheck == masterData.size()) {
                                                                Log.INSTANCE.v(LOG_TAG, "@@@ Done with master sync " + countCheck);


                                                                onComplete.execute(true, null, "Sync is success");

                                                            }
                                                        }
                                                    });
                                                } else {
                                                    Log.INSTANCE.v(LOG_TAG, "@@@ Master table deletion failed for " + tableName);
                                                }
                                            }
                                        });
                                    } else {
                                        dataAccessHandler.insertData(tableName, masterData.get(tableName), new ApplicationThread.OnComplete<String>() {
                                            @Override
                                            public void execute(boolean success, String result, String msg) {
                                                if (success) {
                                                    Log.INSTANCE.v(LOG_TAG, "@@@ sync success for " + tableName);
                                                } else {
                                                    Log.INSTANCE.v(LOG_TAG, "@@@ check 2 " + masterData.size() + "...pos " + countCheck);
                                                    Log.INSTANCE.v(LOG_TAG, "@@@ sync failed for " + tableName + " message " + msg);
                                                }
                                                if (countCheck == masterData.size()) {
                                                    Log.INSTANCE.v(LOG_TAG, "@@@ Done with master sync " + countCheck);
                                                    ProgressBar.Companion.hideProgressBar();
                                                    onComplete.execute(true, null, "Sync is success");
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    } else {
                        ProgressBar.Companion.hideProgressBar();
                        Log.INSTANCE.v(LOG_TAG, "@@@ Sync is up-to-date");
                        onComplete.execute(true, null, "Sync is up-to-date");
                    }
                } else {
                    ProgressBar.Companion.hideProgressBar();
                    onComplete.execute(false, null, "Master sync failed. Please try again");
                }
            }
        });
    }











//    public static void updateSyncDate(Context context, String date) {
////        Log.v(LOG_TAG, "@@@ saving date into");
//        SharedPreferences sharedPreferences = context.getSharedPreferences("appprefs", MODE_PRIVATE);
//        sharedPreferences.edit().putString(PREVIOUS_SYNC_DATE, date).apply();
//    }
//
//    public static void startTransactionSync(final Context context, final ProgressDialogFragment progressDialogFragment) {
//
//        SharedPreferences sharedPreferences = context.getSharedPreferences("appprefs", MODE_PRIVATE);
//        String date = sharedPreferences.getString(PREVIOUS_SYNC_DATE, null);
//
//        final String finalDate =  date;
////        Log.v(LOG_TAG, "@@@ Date " + date);
//        progressDialogFragment.updateText("Getting total records count");
//        final ProgressDialogFragment finalProgressDialogFragment = progressDialogFragment;
//        getCountOfHits(finalDate, new ApplicationThread.OnComplete() {
//            @Override
//            public void execute(boolean success, Object result, String msg) {
//                if (success) {
////                    Log.v(LOG_TAG, "@@@@ count here " + result.toString());
//                    List<DataCountModel> dataCountModelList = (List<DataCountModel>) result;
//                    prepareIndexes(finalDate, dataCountModelList, context, finalProgressDialogFragment);
//                } else {
//                    if (null != finalProgressDialogFragment) {
//                        finalProgressDialogFragment.dismiss();
//                    }
////                    Log.v(LOG_TAG, "Transaction sync failed due to data issue-->"+msg);
//                    UiUtils.showCustomToastMessage("Transaction sync failed due to data issue", context, 1);
//                }
//            }
//        });
//    }
//
//    public static void prepareIndexes(final String date, List<DataCountModel> countData, final Context context, ProgressDialogFragment progressDialogFragment) {
//        if (!countData.isEmpty()) {
//            reverseSyncTransCount = 0;
//            transactionsCheck = 0;
//            dataToUpdate.clear();
//            final DataAccessHandler dataAccessHandler = new DataAccessHandler(context);
//            new DownLoadData(context, date, countData, 0, 0, dataAccessHandler, progressDialogFragment).execute();
//        } else {
////            ProgressBar.hideProgressBar();
//            if (null != progressDialogFragment) {
//                progressDialogFragment.dismiss();
//            }
//
//            CommonUtils.showMyToast("There is no transactions data to sync", context);
//        }
//    }
//
//    public static void getCountOfHits(String date, final ApplicationThread.OnComplete onComplete) {
//        String countUrl = "";
//        LinkedHashMap<String, String> syncDataMap = new LinkedHashMap<>();
//        syncDataMap.put("Date", TextUtils.isEmpty(date) ? "null" : date);
////        syncDataMap.put("UserId", CommonConstants.USER_ID);
////        syncDataMap.put("IsUserDataAccess", CommonConstants.migrationSync);
//        countUrl = Config.live_url + Config.getTransCount;
//        CloudDataHandler.getGenericData(countUrl, syncDataMap, new ApplicationThread.OnComplete<List<DataCountModel>>() {
//            @Override
//            public void execute(boolean success, List<DataCountModel> result, String msg) {
//                onComplete.execute(success, result, msg);
//            }
//        });
//    }
//
//
//    public static class DownLoadData extends AsyncTask<String, String, String> {
//
//        private static final MediaType TEXT_PLAIN = MediaType.parse("application/x-www-form-urlencoded");
//        private Context context;
//        private String date;
//        private List<DataCountModel> totalData;
//        private int totalDataCount;
//        private int currentIndex;
//        private DataAccessHandler dataAccessHandler;
//        private ProgressDialogFragment progressDialogFragment;
//
//
//
//        public DownLoadData(final Context context, final String date, final List<DataCountModel> totalData, int totalDataCount, int currentIndex, DataAccessHandler dataAccessHandler, ProgressDialogFragment progressDialogFragment) {
//            this.context = context;
//            this.totalData = totalData;
//            this.date = date;
//            this.totalDataCount = totalDataCount;
//            this.currentIndex = currentIndex;
//            this.dataAccessHandler = dataAccessHandler;
//            this.progressDialogFragment = progressDialogFragment;
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//            String resultMessage = null;
//            Response response = null;
//            String countUrl = Config.live_url + String.format(Config.getTransData, totalData.get(totalDataCount).getMethodName());
////            Log.v(LOG_TAG, "@@@ data sync url " + countUrl);
//            final String tableName = totalData.get(totalDataCount).getTableName();
//
//            progressDialogFragment.updateText("Downloading " + tableName + " (" + currentIndex + "/" + totalData.get(totalDataCount).getCount() + ")" + " data");
////            if (currentIndex == 0){
////                if (tableName.equalsIgnoreCase("Farmer")){
////                    FarmerDataCount = totalData.get(totalDataCount).getCount();
////                }else if (tableName.equalsIgnoreCase("Plot")){
////                    PlotDataCount = totalData.get(totalDataCount).getCount();
////                }
////            }
//            try {
//                URL obj = new URL(countUrl);
//                Map<String, String> syncDataMap = new LinkedHashMap<>();
//                syncDataMap.put("Date", TextUtils.isEmpty(date) ? "null" : date);
//                syncDataMap.put("UserId", CommonConstants.USER_ID);
//                syncDataMap.put("IsUserDataAccess", CommonConstants.migrationSync);
//                syncDataMap.put("Index", String.valueOf(currentIndex));
//                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
//                con.setRequestMethod("POST");
//                con.setDoInput(true);
//                con.setDoOutput(true);
//                con.setRequestProperty("User-Agent", USER_AGENT);
//
//                final StringBuilder sb = new StringBuilder();
//                boolean first = true;
//                RequestBody requestBody = null;
//                for (Map.Entry<String, String> entry : syncDataMap.entrySet()) {
//                    if (first) first = false;
//                    else sb.append("&");
//
//                    sb.append(URLEncoder.encode(entry.getKey(), HTTP.UTF_8)).append("=")
//                            .append(URLEncoder.encode(entry.getValue().toString(), HTTP.UTF_8));
//
////                    Log.d(LOG_TAG, "\nposting key: " + entry.getKey() + " -- value: " + entry.getValue());
//                }
//                requestBody = RequestBody.create(TEXT_PLAIN, sb.toString());
//
//                Request request = HttpClient.buildRequest(countUrl, "POST", (requestBody != null) ? requestBody : RequestBody.create(TEXT_PLAIN, "")).build();
//                OkHttpClient client = getOkHttpClient();
//                response = client.newCall(request).execute();
//                int statusCode = response.code();
//
//                final String strResponse = response.body().string();
//
//
////                Log.d(LOG_TAG, " ############# POST RESPONSE ################ (" + statusCode + ")\n\n" + strResponse + "\n\n");
//                JSONArray dataArray = new JSONArray(strResponse);
//
//                if (statusCode == HttpURLConnection.HTTP_OK) {
//
////                    if (TextUtils.isEmpty(date)) {
////                        if (tableName.equalsIgnoreCase(DatabaseKeys.TABLE_COMPLAINTTYPEXREF)) {
////                            Log.v(LOG_TAG, "@@@@ Data insertion status comp ");
////                        } else if (tableName.equalsIgnoreCase(DatabaseKeys.TABLE_COMPLAINTREPOSITORY)) {
////                            Log.v(LOG_TAG, "@@@@ Data insertion status comp2 ");
////                        }
//                    List dataToInsert = new ArrayList();
//                    for (int i = 0; i < dataArray.length(); i++) {
//                        JSONObject eachDataObject = dataArray.getJSONObject(i);
//                        dataToInsert.add(CommonUtils.toMap(eachDataObject));
//                    }
//                    dataAccessHandler.insertData(tableName, dataToInsert, new ApplicationThread.OnComplete<String>() {
//                        @Override
//                        public void execute(boolean success, String result, String msg) {
//                            if (success) {
////                                    Log.v(LOG_TAG, "@@@@ Data insertion status " + result);
//                                if (tableName.equalsIgnoreCase(DatabaseKeys.TABLE_TRAIL)) {
//                                    if (currentIndex == 0) {
//                                        trail = 1;
//                                    } else {
//                                        trail = trail + 1;
//                                    }
//                                } else if (tableName.equalsIgnoreCase(DatabaseKeys.TABLE_PROMOTIONAL_PLAN)) {
//                                    if (currentIndex == 0) {
//                                        promotionalPlan = 1;
//                                    } else {
//                                        promotionalPlan = promotionalPlan + 1;
//                                    }
//                                } else if (tableName.equalsIgnoreCase(DatabaseKeys.TABLE_ADVANCE_TOUR_PLAN)) {
//                                    if (currentIndex == 0) {
//                                        tourPlan = 1;
//                                    } else {
//                                        tourPlan = tourPlan + 1;
//                                    }
//                                }
//                            } else {
////                                    Log.v(LOG_TAG, "@@@@ Data insertion Failed In Table-"+tableName+"Due to" + result);
//                            }
//
//                        }
//                    });
//                } else {
//                    if (tableName.equalsIgnoreCase(DatabaseKeys.TABLE_PROMOTIONAL_PLAN)) {
//                        Gson gson = new Gson();
//                        Type type = new TypeToken<List<PromotionalPlan>>() {
//                        }.getType();
//                        List<PromotionalPlan> fileRepositoryInnerList = gson.fromJson(dataArray.toString(), type);
//                        if (null != fileRepositoryInnerList && fileRepositoryInnerList.size() > 0)
//                            dataToUpdate.put(tableName, fileRepositoryInnerList);
//                    } else if (tableName.equalsIgnoreCase(DatabaseKeys.TABLE_ADVANCE_TOUR_PLAN)) {
//                        Gson gson = new Gson();
//                        Type type = new TypeToken<List<AdvanceTourPlan>>() {
//                        }.getType();
//                        List<AdvanceTourPlan> addressDataList = gson.fromJson(dataArray.toString(), type);
//                        if (null != addressDataList && addressDataList.size() > 0)
//                            dataToUpdate.put(tableName, addressDataList);
//                    } else if (tableName.equalsIgnoreCase(DatabaseKeys.TABLE_TRAIL)) {
//                        Gson gson = new Gson();
//                        Type type = new TypeToken<List<Trial>>() {
//                        }.getType();
//                        List<Trial> farmerDataList = gson.fromJson(dataArray.toString(), type);
//                        if (null != farmerDataList && farmerDataList.size() > 0)
//                            dataToUpdate.put(tableName, farmerDataList);
//                    }
//                }
//            } catch (ProtocolException exception) {
//                exception.printStackTrace();
//            } catch (IOException exception) {
//                exception.printStackTrace();
//            } catch (JSONException exception) {
//                exception.printStackTrace();
//            }
//            resultMessage = "success";
//            return resultMessage;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            currentIndex++;
//            if (currentIndex == totalData.get(totalDataCount).getCount()) {
//                currentIndex = 0;
//                totalDataCount++;
//                if (totalDataCount == totalData.size()) {
////                    Log.v(LOG_TAG, "@@@ done with data syncing");
//                    if (TextUtils.isEmpty(date)) {
////                        ProgressBar.hideProgressBar();
//                        if (null != progressDialogFragment && !CommonUtils.currentActivity.isFinishing()) {
//                            progressDialogFragment.dismiss();
//
//                        }
////                        Integer resetFarmerCount = Integer.parseInt(dataAccessHandler.getOnlyOneValueFromDb(Queries.getInstance().getFarmerCount()));
////                        Integer resetPlotCount = Integer.parseInt(dataAccessHandler.getOnlyOneValueFromDb(Queries.getInstance().getPlotCount()));
//
//                        if ((trail == 1)&& (tourPlan == 1) && (promotionalPlan == 1)){
//                            if (trail != 0 && promotionalPlan != 0 && promotionalPlan != 0 ){
//
//                                UiUtils.showCustomToastMessage("Data synced successfully", context, 0);
//                                updateSyncDate(context, CommonUtils.getcurrentDateTime(CommonConstants.DATE_FORMAT_DDMMYYYY_HHMMSS));
//
//                            }else {
//                                UiUtils.showCustomToastMessage("Data is not Synced Properly Again its DownLoading the Data " , context, 1);
//                                if (CommonUtils.isNetworkAvailable(context)){
//                                    updateSyncDate(context, null);
//                                    for (String s : RefreshSynxActivity.allRefreshDataMap) {
//                                        dataAccessHandler.executeRawQuery("DELETE FROM " + s);
////                                        Log.v(LOG_TAG,"delete table"+s);
//                                    }
//                                    progressDialogFragment = new ProgressDialogFragment();
//                                    startTransactionSync(context, progressDialogFragment);
//                                } else {
//                                    UiUtils.showCustomToastMessage("Please check network connection", context, 1);
//                                }
//
//                            }
//
//                        }else {
//                            UiUtils.showCustomToastMessage("Data is not Synced Properly Again its DownLoading the Data", context, 1);
//                            if (CommonUtils.isNetworkAvailable(context)){
//                                updateSyncDate(context, null);
//                                for (String s : RefreshSynxActivity.allRefreshDataMap) {
//                                    dataAccessHandler.executeRawQuery("DELETE FROM " + s);
////                                    Log.v(LOG_TAG,"delete table"+s);
//                                }
//                                progressDialogFragment = new ProgressDialogFragment();
//                                startTransactionSync(context, progressDialogFragment);
//
//                            }else {
//                                UiUtils.showCustomToastMessage("Please check network connection", context, 1);
//                            }
//                        }
//
//                    } else {
//                        reverseSyncTransCount = 0;
//                        Set tableNames = dataToUpdate.keySet();
//                        List<String> tableNamesList = new ArrayList();
//                        tableNamesList.addAll(tableNames);
//                        updateTransactionData(dataToUpdate, dataAccessHandler, tableNamesList, progressDialogFragment, new ApplicationThread.OnComplete() {
//                            @Override
//                            public void execute(boolean success, Object result, String msg) {
//                                if (success) {
//                                    updateSyncDate(context, CommonUtils.getcurrentDateTime(CommonConstants.DATE_FORMAT_DDMMYYYY_HHMMSS));
//                                    UiUtils.showCustomToastMessage("Data synced successfully", context, 0);
//                                } else {
//                                    UiUtils.showCustomToastMessage(msg, context, 1);
//                                }
//                                if (null != progressDialogFragment && !CommonUtils.currentActivity.isFinishing()) {
//                                    progressDialogFragment.dismiss();
//                                }
//                            }
//                        });
//                    }
//                } else {
////                    Log.v(LOG_TAG, "@@@ data downloading next count " + currentIndex + " out of " + totalData.size());
//                    new DownLoadData(context, date, totalData, totalDataCount, currentIndex, dataAccessHandler, progressDialogFragment).execute();
//                }
//            } else {
////                Log.v(LOG_TAG, "@@@ data downloading next count " + currentIndex + " out of " + totalData.size());
//                new DownLoadData(context, date, totalData, totalDataCount, currentIndex, dataAccessHandler, progressDialogFragment).execute();
//            }
//        }
//    }
//
//    public static synchronized void updateTransactionData(final LinkedHashMap<String, List> transactionsData, final DataAccessHandler dataAccessHandler, final List<String> tableNames, final ProgressDialogFragment progressDialogFragment, final ApplicationThread.OnComplete onComplete) {
//        progressDialogFragment.updateText("Updating data...");
//        if (transactionsData != null && transactionsData.size() > 0) {
////            Log.v(LOG_TAG, "@@@ Transactions sync is success and data size is " + transactionsData.size());
//            final String tableName = tableNames.get(reverseSyncTransCount);
//            innerCountCheck = 0;
//            updateDataIntoDataBase(transactionsData, dataAccessHandler, tableName, new ApplicationThread.OnComplete() {
//                @Override
//                public void execute(boolean success, Object result, String msg) {
//                    if (success) {
//                        reverseSyncTransCount++;
//                        if (reverseSyncTransCount == transactionsData.size()) {
//                            onComplete.execute(success, "data updated successfully", "");
//                        } else {
//                            updateTransactionData(transactionsData, dataAccessHandler, tableNames, progressDialogFragment, onComplete);
//                        }
//                    } else {
//                        reverseSyncTransCount++;
//                        if (reverseSyncTransCount == transactionsData.size()) {
//                            onComplete.execute(success, "data updated successfully", "");
//                        } else {
//                            updateTransactionData(transactionsData, dataAccessHandler, tableNames, progressDialogFragment, onComplete);
//                        }
//                    }
//                }
//            });
//        } else {
//            onComplete.execute(false, "data not found to save", "");
//        }
//    }
//
//
//    private static synchronized void updateDataIntoDataBase(final LinkedHashMap<String, List> transactionsData, final DataAccessHandler dataAccessHandler, final String tableName, final ApplicationThread.OnComplete onComplete) {
//        final List dataList = transactionsData.get(tableName);
//        List dataToInsert = new ArrayList();
//        JSONObject ccData = null;
//        Gson gson = new GsonBuilder().serializeNulls().create();
//
//        boolean recordExisted = false;
//        String whereCondition = null;
//
//        if (dataList.size() > 0) {
//            if (tableName.equalsIgnoreCase(DatabaseKeys.TABLE_TRAIL)) {
//                Trial trial = (Trial) dataList.get(innerCountCheck);
////                whereCondition = " where  Code = '" + fileRepository.getFarmercode() + "'";
////                recordExisted = dataAccessHandler.checkValueExistedInDatabase(Queries.getInstance().checkRecordStatusInTable(tableName, "FarmerCode", fileRepository.getFarmercode()));
//            } else if (tableName.equalsIgnoreCase(DatabaseKeys.TABLE_PROMOTIONAL_PLAN)) {
//                PromotionalPlan promotionalPlan = (PromotionalPlan) dataList.get(innerCountCheck);
//                promotionalPlan.setServerStatus(1);
////                whereCondition = " where  Code = '" + addressData.getCode() + "'";
//                try {
//                    ccData = new JSONObject(gson.toJson(promotionalPlan));
//                    dataToInsert.add(CommonUtils.toMap(ccData));
//                } catch (JSONException e) {
////                    Log.e(LOG_TAG, "####" + e.getLocalizedMessage());
//                }
////                recordExisted = dataAccessHandler.checkValueExistedInDatabase(Queries.getInstance().checkRecordStatusInTable(tableName, "Code", addressData.getCode()));
//            } else if (tableName.equalsIgnoreCase(DatabaseKeys.TABLE_ADVANCE_TOUR_PLAN)) {
//                AdvanceTourPlan advanceTourPlan = (AdvanceTourPlan) dataList.get(innerCountCheck);
//                advanceTourPlan.setServerStatus(1);
////                whereCondition = " where  Code = '" + farmerData.getCode() + "'";
//                try {
//                    ccData = new JSONObject(gson.toJson(advanceTourPlan));
//                    dataToInsert.add(CommonUtils.toMap(ccData));
////                    recordExisted = dataAccessHandler.checkValueExistedInDatabase(Queries.getInstance().checkRecordStatusInTable(tableName, "Code", farmerData.getCode()));
//                } catch (JSONException e) {
////                    Log.e(LOG_TAG, "####" + e.getLocalizedMessage());
////                }
//            }
//            if (dataList.size() != innerCountCheck) {
//                updateOrInsertData(tableName, dataToInsert, whereCondition, recordExisted, dataAccessHandler, new ApplicationThread.OnComplete() {
//                    @Override
//                    public void execute(boolean success, Object result, String msg) {
//                        innerCountCheck++;
//                        if (innerCountCheck == dataList.size()) {
//                            innerCountCheck = 0;
//                            onComplete.execute(true, "", "");
//                        } else {
//                            updateDataIntoDataBase(transactionsData, dataAccessHandler, tableName, onComplete);
//                        }
//                    }
//                });
//            } else {
//                onComplete.execute(true, "", "");
//            }
//        } else {
//            innerCountCheck++;
//            if (innerCountCheck == dataList.size()) {
//                innerCountCheck = 0;
//                onComplete.execute(true, "", "");
//            } else {
//                updateDataIntoDataBase(transactionsData, dataAccessHandler, tableName, onComplete);
//            }
//        }
//
//    }
//
//
//
//
//
//}
//
//    private static void updateOrInsertData(final String tableName, List dataToInsert, String whereCondition, boolean recordExisted, DataAccessHandler dataAccessHandler, final ApplicationThread.OnComplete onComplete) {
//        if (recordExisted) {
//            dataAccessHandler.updateData(tableName, dataToInsert, true, whereCondition, new ApplicationThread.OnComplete<String>() {
//                @Override
//                public void execute(boolean success, String result, String msg) {
//                    onComplete.execute(success, null, "Sync is " + success + " for " + tableName);
//                }
//            });
//        } else {
//            dataAccessHandler.insertData(tableName, dataToInsert, new ApplicationThread.OnComplete<String>() {
//                @Override
//                public void execute(boolean success, String result, String msg) {
//                    onComplete.execute(true, null, "Sync is " + success + " for " + tableName);
//                }
//            });
//        }
//    }

}