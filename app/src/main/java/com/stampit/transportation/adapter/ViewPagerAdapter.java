package com.stampit.transportation.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.stampit.transportation.fragments.LicenseImageViewFragment;
import com.stampit.transportation.fragments.FragmentB;


public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {

        } else if (position == 1) {
            fragment = new FragmentB();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Load";
        } else if (position == 1) {
            title = "Unload";
        }


        return title;
    }
}
