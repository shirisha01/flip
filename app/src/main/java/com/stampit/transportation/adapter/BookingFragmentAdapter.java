package com.stampit.transportation.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.stampit.hmcsales.cloudhelper.Log;
import com.stampit.transportation.R;
import com.stampit.transportation.activity.OnGoingTripViewDetailsActivity;
import com.stampit.transportation.maps.MapsActivityCurrentPlace;
import com.stampit.transportation.model.Booking;

import java.util.ArrayList;

public class BookingFragmentAdapter extends RecyclerView.Adapter<BookingFragmentAdapter.SetView> {
    private ArrayList<Booking> bookingArrayList;

    private static final int TYPE_ROW = 0;
    private static final int TYPE_ROW_COLORFUL = 1;

    private Context mContext;

    public BookingFragmentAdapter(ArrayList<Booking> bookingArrayList, Context mContext) {
        this.bookingArrayList = bookingArrayList;

        this.mContext = mContext;
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 0) {
            return TYPE_ROW_COLORFUL;
        }

        return TYPE_ROW;
    }

    @NonNull
    @Override
    public SetView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ROW) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_blue_booking_layout,
                    parent, false);
            return new SetView(view);
        }else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_orange_booking, parent, false);


            return new SetView(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull SetView holder, int position) {
        final Booking p = bookingArrayList.get(position);

        int pos = position + 1;
        holder.tripCount.setText("Trip#" + pos + " ");
        holder.sourcePoint.setText("" + p.getLoadingAddress());
        holder.destinationPoint.setText("" + p.getDeliveryAddress());
        holder.dataTime.setText("Date : " + p.getServiceRequiredDate());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(mContext, MapsActivityCurrentPlace.class);
                intent.putExtra("DriverId",p.getId());

                mContext.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return bookingArrayList.size();
    }
    public void addItems(ArrayList<Booking> pcs) {
        this.bookingArrayList = pcs;
        notifyDataSetChanged();
    }


    public class SetView extends RecyclerView.ViewHolder {
        TextView tripCount, sourcePoint, destinationPoint, dataTime;
         CardView cardView;

        public SetView(@NonNull View itemView) {
            super(itemView);
            tripCount = itemView.findViewById(R.id.tripCount);
            sourcePoint = itemView.findViewById(R.id.sourcePoint);
            destinationPoint = itemView.findViewById(R.id.destinationPoint);
            dataTime = itemView.findViewById(R.id.dataTime);
            cardView=itemView.findViewById(R.id.cardView);


        }
    }
}
